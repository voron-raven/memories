from django.db import models
from django.contrib.auth.models import User
from django.forms import ModelForm
from django.utils.translation import ugettext as _
from datetime import date 

class Profile(models.Model):
	"""Profile model"""
	user = models.ForeignKey(User, unique=True)
	street_address_1 = models.CharField(max_length=80, verbose_name=_('Street address 1'))
	street_address_2 = models.CharField(max_length=80, verbose_name=_('Street address 2'))
	city = models.CharField(max_length=80, verbose_name=_('City'))
	state = models.CharField(max_length=80, verbose_name=_('State / Province'))
	country = models.CharField(max_length=80, verbose_name=_('Country'))
	primary_phone_number = models.CharField(max_length=15, verbose_name=_('primary phone number'))
	secondary_phone_number = models.CharField(max_length=15, verbose_name=_('secondary phone number'))
	is_seller = models.BooleanField(default=False)
	company = models.CharField(max_length=40, verbose_name=_('Company'))
	
	avatar = models.ImageField("Profile Pic", upload_to="images/", blank=True, null=True)
	gender = models.BooleanField(default=False)
	birthday = models.DateField(_('Date'), default=date.today)
 	telephone = models.CharField(max_length=80, verbose_name=_('Telephone number'))
 	skype = models.CharField(max_length=80, verbose_name=_('Skype'))

class Category(models.Model):
	""" Category model """
	name = models.CharField(max_length=80, verbose_name=_('Category Name'))
	description = models.TextField()

class Company(models.Model):
 	""" Company Profile model """
 	name = models.CharField(max_length=80, verbose_name=_('Company Name'))
 	logo = models.ImageField("Profile Pic", upload_to="images/", blank=True, null=True)
 	telephone = models.CharField(max_length=80, verbose_name=_('Telephone number'))
 	email = models.EmailField(max_length=80)
 	reputation = models.PositiveIntegerField(default=0)
 	description = models.TextField()
 	category = models.ForeignKey(Category)

class ProfileForm(ModelForm):
    """ProfileForm model"""
    class Meta:
        model = Profile
        fields = ['street_address_1', 'street_address_2', 'city', 'state', 'country', 'primary_phone_number', 'secondary_phone_number']
