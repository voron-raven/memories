from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns('',
    url(r'^login$', 'customer.views.login', name='login'),
    url(r'^logout$', 'customer.views.logout', name='logout'),

    url(r'^admin/', include(admin.site.urls)),
)
